In order to run this api you'll need to have:
 - Java JDK 18
 - An IDE to run the project (I recommend INTELIJ).
 - Execute Maven's command "mvn clean package" in order to reload the dependencies used in the project.
 - The most recent PostgreSQL server (pgAdmin 4 recommended). Create a custom Database. 
 
SETTING UP THE DATABASE

When you run the api for the first time, make sure the file 'application.properties' looks like this:

    spring.jpa.database=POSTGRESQL
    spring.datasource.url=jdbc:postgresql://localhost:5432/{yourDataBaseName}
    spring.datasource.username={yourServerUser}
    spring.datasource.password={yourPassword}
    spring.datasource.driver.class=org.postgresql.Driver
    spring.jpa.show-sql=true
    spring.jpa.hibernate.ddl-auto=update
    spring.jpa.generate-ddl=true

This will create the necessary tables for the api to work. After that, comment the line 'spring.jpa.generate-ddl=true'
so it shows like this:

    spring.jpa.database=POSTGRESQL
    spring.datasource.url=jdbc:postgresql://localhost:5432/{yourDataBaseName}
    spring.datasource.username={yourServerUser}
    spring.datasource.password={yourPassword}
    spring.datasource.driver.class=org.postgresql.Driver
    spring.jpa.show-sql=true
    spring.jpa.hibernate.ddl-auto=update
    #spring.jpa.generate-ddl=true

Execute the following scripts on you database:

    INSERT INTO public.department(id, name) VALUES (1, 'Finnances'), (2, 'IT');
    INSERT INTO public.document_type(id, name) VALUES (1, 'ID'), (2, 'Passport');

TESTING

To test the api you will need a tool to make http petitions to a server, such as Postman.
Next there are some example JSON to test different services:

Create a new employee
```json
{
    "firstLastName": "MARCANO",
    "secondLastName": "FUNES",
    "firstName" : "GUILLERMO",
    "otherName" : "ARTURO",
    "country" : "Colombia",
    "documentType" : {
      "id" : 1,
      "documentName": "ID"
    },
    "documentNumber" : "123456",
    "entryDate": "2022-07-30",
    "department": {
      "id": 1,
      "departmentName": "Finances"
    }
}
```

Update an existing employee
```json
{
    "id": 1,
    "firstLastName": "MARCANO",
    "secondLastName": "FUNES",
    "firstName" : "GUILLERMO",
    "otherName" : "ARTURO",
    "country" : "Colombia",
    "documentType" : {
        "id" : 1,
        "documentName": "ID"
      },
    "documentNumber" : "123456",
    "entryDate": "2022-07-30",
    "department": {
        "id": 2,
        "departmentName": "IT"
    }
}
```

