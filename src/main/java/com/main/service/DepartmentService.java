package com.main.service;

import com.main.model.Department;

import java.util.List;

public interface DepartmentService {

    List<Department> getAll();

}
