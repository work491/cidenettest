package com.main.service;

import com.main.model.Document;

import java.util.List;

public interface DocumentService {

    List<Document> getAll();
    Document getDocument(int id);
}
