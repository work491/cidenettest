package com.main.service;

import com.main.model.Employee;
import com.main.repository.EmployeeRepository;
import com.main.service.utils.NameValidators;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class EmployeeServiceImpl implements EmployeeService {

    public static final String COLOMBIA = "Colombia";
    public static final String US = "United States";
    public static final String DOMAINCO = "@cidenet.com.co";
    public static final String DOMAINUS = "@cidenet.com.us";
    public static final String ACTIVE = "Active";

    @Autowired
    private EmployeeRepository employeeRepository;

    @Override
    public List<Employee> getAll() {
        return employeeRepository.findAll();
    }

    @Override
    public Employee saveEmployee(Employee employee) {
        employee.setEmail(createEmail(employee));
        if (!validateEntryDateBefore(employee) || !validateEntryDateAfter(employee)) {
            throw new IllegalArgumentException("The entry date must be before the current date");
        }
        if (!validateFirstLastName(employee.getFirstLastName()) || !validateFirstName(employee.getFirstName()) ||  !validateSecondLastName(employee.getSecondLastName())){
            throw new IllegalArgumentException("Solo permite caracteres de la A a la Z, mayúscula, sin acentos ni Ñ. Es requerido y su longitud máxima será de 20 letras");
        }
        if(!validateOtherName(employee.getOtherName())){
            throw new IllegalArgumentException("Solo permite caracteres de la A a la Z, mayúscula, sin acentos ni Ñ. Es requerido y su longitud máxima será de 50 letras");
        }
        employee.setStatus(ACTIVE);
        employee.setRegistrationDate(new Date());
        return employeeRepository.save(employee);
    }

    private boolean validateFirstLastName(String lastName){
      return  NameValidators.validateAlphabetical(lastName) && NameValidators.validateLength20(lastName);
    }

    private boolean validateSecondLastName(String lastName){
        return  NameValidators.validateAlphabetical(lastName) && NameValidators.validateLength20(lastName);
    }

    private boolean validateFirstName(String name){
        return  NameValidators.validateAlphabetical(name) && NameValidators.validateLength20(name);
    }

    private boolean validateOtherName(String otherName){
        return  NameValidators.validateAlphabetical(otherName) && NameValidators.validateLength50(otherName);
    }
    private String createEmail(Employee employee) {
        String email = "";
        List<Employee> employeeList = employeeRepository.findAll();
        Long occurrences = employeeList.stream().filter(employee1 -> employee1.getFirstLastName().equalsIgnoreCase(employee.getFirstLastName()))
                .filter(employee1 -> employee1.getFirstName().equalsIgnoreCase(employee.getFirstName())).count();
        if (employee.getCountry().equalsIgnoreCase(COLOMBIA)) {
            if (occurrences > 0) {
                email = getEmailWithOccurrences(employee, DOMAINCO, occurrences);
            } else {
                email = getEmail(employee, DOMAINCO);
            }
        } else if (employee.getCountry().equalsIgnoreCase(US)) {
            if (occurrences > 0) {
                email = getEmailWithOccurrences(employee, DOMAINUS, occurrences);
            } else {
                email = getEmail(employee, DOMAINUS);
            }
        } else {
            throw new IllegalArgumentException("The country must be Colombia or the United States");
        }
        return email;
    }

    private static String getEmailWithOccurrences(Employee employee, String domain, Long occurrences) {
        return employee.getFirstName().replaceAll("\\s+", "").toLowerCase() + "." +
                employee.getFirstLastName().replaceAll("\\s+", "").toLowerCase() + "." + occurrences + domain;
    }

    private static String getEmail(Employee employee, String domain) {
        return employee.getFirstName().replaceAll("\\s+", "").toLowerCase() + "." +
                employee.getFirstLastName().replaceAll("\\s+", "").toLowerCase() + domain;
    }

    private static boolean validateEntryDateBefore(Employee employee) {
        return employee.getEntryDate().before(new Date());
    }

    private static boolean validateEntryDateAfter(Employee employee) {
        LocalDate date = LocalDate.now();
        return employee.getEntryDate().after(Date.from(date.minus(1, ChronoUnit.MONTHS).atStartOfDay().atZone(ZoneId.systemDefault()).toInstant()));
    }

    @Override
    public Employee updateEmployee(Employee employee) {
        Employee currentEmployee = getById(employee.getId());
        if (!currentEmployee.getFirstName().equalsIgnoreCase(employee.getFirstName()) ||
                !currentEmployee.getFirstLastName().equalsIgnoreCase(employee.getFirstLastName()) ||
                !currentEmployee.getCountry().equalsIgnoreCase(employee.getCountry())) {
            employee.setEmail(createEmail(employee));
        } else {
            employee.setEmail(currentEmployee.getEmail());
        }
        employee.setRegistrationDate(currentEmployee.getRegistrationDate());
        employee.setStatus(currentEmployee.getStatus());
        return employeeRepository.save(employee);
    }

    @Override
    public Employee getById(int id) {
        return employeeRepository.findById(id).get();
    }

    @Override
    public List<Employee> getByFirstName(String firstName) {
        System.out.println(firstName);
        return employeeRepository.findAll()
                .stream()
                .filter(employee1 -> employee1.getFirstName().equalsIgnoreCase(firstName))
                .collect(Collectors.toList());
    }

    @Override
    public List<Employee> getByOtherName(String otherName) {
        return employeeRepository.findAll().stream().filter(employee1 -> employee1.getOtherName().equalsIgnoreCase(otherName)).toList();
    }

    @Override
    public List<Employee> getByFirstLastName(String firstLastName) {
        return employeeRepository.findAll().stream().filter(employee1 -> employee1.getFirstLastName().equalsIgnoreCase(firstLastName)).toList();
    }

    @Override
    public List<Employee> getBySecondLastName(String secondLastName) {
        return employeeRepository.findAll().stream().filter(employee1 -> employee1.getSecondLastName().equalsIgnoreCase(secondLastName)).toList();
    }

    @Override
    public List<Employee> getByDocumentType(int documentType) {
        return employeeRepository.findAll().stream().filter(employee1 -> employee1.getDocumentType().getId() == documentType).toList();
    }

    @Override
    public List<Employee> getByDocumentNumber(String documentNumber) {
        return employeeRepository.findAll().stream().filter(employee1 -> employee1.getDocumentNumber().equalsIgnoreCase(documentNumber)).toList();
    }

    @Override
    public List<Employee> getByCountry(String country) {
        return employeeRepository.findAll().stream().filter(employee1 -> employee1.getCountry().equalsIgnoreCase(country)).toList();
    }

    @Override
    public List<Employee> getByEmail(String email) {
        return employeeRepository.findAll().stream().filter(employee1 -> employee1.getEmail().equalsIgnoreCase(email)).toList();
    }

    @Override
    public List<Employee> getByStatus(String status) {
        return employeeRepository.findAll().stream().filter(employee1 -> employee1.getStatus().equalsIgnoreCase(status)).toList();
    }
}
