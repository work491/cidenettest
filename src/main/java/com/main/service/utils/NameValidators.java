package com.main.service.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class NameValidators {

    public static final String REGEX_ALPHABETICAL = "^[A-Z\s]*$";

    public static boolean validateAlphabetical(String name) {
        Pattern c = Pattern.compile(REGEX_ALPHABETICAL);
        Matcher m = c.matcher(name);
        return m.matches();
    }

    public static boolean validateLength20(String name) {
        return name.length() <= 20;
    }

    public static boolean validateLength50(String name) {
        return name.length() <= 50;
    }

}
