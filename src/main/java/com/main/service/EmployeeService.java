package com.main.service;

import com.main.model.Employee;

import java.util.List;

public interface EmployeeService {

    List<Employee> getAll();
    Employee saveEmployee(Employee employee);
    Employee updateEmployee(Employee employee);
    Employee getById(int id);
    List<Employee> getByFirstName(String firstName);
    List<Employee> getByOtherName(String otherName);
    List<Employee> getByFirstLastName(String firstLastName);
    List<Employee> getBySecondLastName(String secondLastName);
    List<Employee> getByDocumentType(int documentType);
    List<Employee> getByDocumentNumber(String documentNumber);
    List<Employee> getByCountry(String country);
    List<Employee> getByEmail(String email);
    List<Employee> getByStatus(String status);

}
