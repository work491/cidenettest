package com.main.controller;

import com.main.model.Document;
import com.main.service.DocumentServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/document")
public class DocumentController {

    @Autowired
    private DocumentServiceImpl documentServiceImpl;

    @GetMapping("/list")
    public ResponseEntity<List<Document>> listDocument() {
        return ResponseEntity.ok(documentServiceImpl.getAll());
    }

}
