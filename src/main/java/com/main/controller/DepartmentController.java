package com.main.controller;

import com.main.model.Department;
import com.main.service.DepartmentServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/department")
public class DepartmentController {

    @Autowired
    private DepartmentServiceImpl departmentServiceImpl;

    @GetMapping("/list")
    public ResponseEntity<List<Department>> listDepartment() {
        return ResponseEntity.ok(departmentServiceImpl.getAll());
    }
}
