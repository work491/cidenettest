package com.main.controller;

import com.main.model.Employee;
import com.main.service.EmployeeServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/employee")
public class EmployeeController {

    @Autowired
    private EmployeeServiceImpl employeeServiceImpl;

    @GetMapping("/list")
    public ResponseEntity<List<Employee>> listEmployees() {
        return ResponseEntity.ok(employeeServiceImpl.getAll());
    }

    @PostMapping("/save")
    public ResponseEntity<Employee> saveEmployee(@Valid @RequestBody Employee employee) {
        return ResponseEntity.ok(employeeServiceImpl.saveEmployee(employee));
    }

    @PostMapping("/update")
    public ResponseEntity<Employee> updateEmployee(@RequestBody Employee employee) {
        return ResponseEntity.ok(employeeServiceImpl.updateEmployee(employee));
    }

    @GetMapping("/list/firstname/{firstName}")
    public ResponseEntity<List<Employee>> listEmployeesByFirstName(@PathVariable String firstName) {
        return ResponseEntity.ok(employeeServiceImpl.getByFirstName(firstName));
    }

    @GetMapping("/list/othername/{otherName}")
    public ResponseEntity<List<Employee>> listEmployeesByOtherName(@PathVariable String otherName) {
        return ResponseEntity.ok(employeeServiceImpl.getByOtherName(otherName));
    }

    @GetMapping("/list/firstlastname/{firstLastName}")
    public ResponseEntity<List<Employee>> listEmployeesByFirstLastName(@PathVariable String firstLastName) {
        return ResponseEntity.ok(employeeServiceImpl.getByFirstLastName(firstLastName));
    }

    @GetMapping("/list/secondlastname/{secondLastName}")
    public ResponseEntity<List<Employee>> listEmployeesBySecondLastName(@PathVariable String secondLastName) {
        return ResponseEntity.ok(employeeServiceImpl.getBySecondLastName(secondLastName));
    }

    @GetMapping("/list/documenttype/{documentType}")
    public ResponseEntity<List<Employee>> listEmployeesByDocumentType(@PathVariable int documentType) {
        return ResponseEntity.ok(employeeServiceImpl.getByDocumentType(documentType));
    }

    @GetMapping("/list/documentnumber/{documentNumber}")
    public ResponseEntity<List<Employee>> listEmployeesByDocumentNumber(@PathVariable String documentNumber) {
        return ResponseEntity.ok(employeeServiceImpl.getByDocumentNumber(documentNumber));
    }

    @GetMapping("/list/country/{country}")
    public ResponseEntity<List<Employee>> listEmployeesByCountry(@PathVariable String country) {
        return ResponseEntity.ok(employeeServiceImpl.getByCountry(country));
    }

    @GetMapping("/list/email/{email}")
    public ResponseEntity<List<Employee>> listEmployeesByEmail(@PathVariable String email) {
        return ResponseEntity.ok(employeeServiceImpl.getByEmail(email));
    }

    @GetMapping("/list/satus/{status}")
    public ResponseEntity<List<Employee>> listEmployeesByStatus(@PathVariable String status) {
        return ResponseEntity.ok(employeeServiceImpl.getByStatus(status));
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String, String> exceptionHandler(MethodArgumentNotValidException e) {
        Map<String, String> exceptions = new HashMap<>();
        e.getBindingResult().getAllErrors().forEach((error) -> {
            String field = ((FieldError) error ).getField();
            String message = error.getDefaultMessage();
            exceptions.put(field, message);
        });
        return exceptions;
    }

}
